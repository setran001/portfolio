$(document).ready(function () {
    $("#content").hide();

    $("#home").click(function() {
        var el = "<h1>hi<span id='color'>.</span></h1><br />" + 
        "<h5> Welcome to my portfolio</h5><h5>" +
        "where I will display random </h5>" + "<h5>samples of my work. </h5>";
        renderContent(el);
    });

    $("#about").click(function() { 
        var el = "<h1>about<span id='color'>.</span></h1><br />" + 
        " <p>This portfolio is a capstone assignment for class BIMD 233 of winter quarter at University of Washington Bothell. This class is called:" + 
        "Fundamentals of Web Design and is aimed at teaching HTML, CSS, and JavaScript. This class overall has some very useful information and I have" +
         "learn many things. Prior to taking this class, I had no experience in making websites. There were twenty labs in total and all were interesting " + 
         "to work with. Some of my strengths were in understanding the structure of html and using ids and classes with tags to manipulate them with CSS and " + 
         "JavaScript directly. My understanding in CSS is still developing since I realized that there were tons of things that can be used to add more style to webpages.</p>" + 
         "<p>My biggest struggle in BIMD233 was learning about JavaScript and applying it to HTML and CSS. I kept running into errors that are computer system realated when trying " +
          "to get data with Ajax. I also had trouble with using Jquery, but I eventually find solutions by do web searches. Besides everything, each time I practice using JavaScript " + 
          "I feel that I am getting better at understanding it. One of my strengths in JavaScript is manipulating HTML elements through Jquery. A weakness for me lies in understanding how to " + 
          "incorporate JavaScript, Jquery, and other things like Ajax together logically. Overall, I find that the skills that I learned in BIMD233 will help me in the future of design.</p>" +
          "<p>My primary source of inspiration is minimalism, which is something that I am striving for since I am naturally messy when it comes to organization.</p>";
        renderContent(el);
    });

    $("#work").click(function() {
        var el = "<h1>work<span id='color'>.</span></h1><br />" + 
        "<p>check them out :D</p>";
        renderContent(el);
    });

    $("#photos").click(function() {
        var el ="<h1>photo album<span id='color'>.</span></h1>" + "<p>theme: dark</p>";
        var images = "<img src='image/brothersister.jpg' id='pic'> " +
        "<img src='image/behindfence.jpg' id='pic'>" + "<img src='image/walkingtothebathroom.jpg' id='pic'>" + 
        "<img src='image/lonelycow.jpg' id='pic'>" + "<img src='image/link.jpg' id='pic'>" +
        "<img src='image/truckonfire.jpg' id='pic'>";
        el = el + images;
        renderContent(el);
        $( "#pic" ).mouseover();
    })

    $("#posters").click(function() {
        var el ="<h1>posters<span id='color'>.</span></h1><br /><p>more to come...</p>";
        var images = "<img src='image/poster/workps.jpg' width=50% id='poster'>"  ;
        el = el + images;
        renderContent(el);
    });

    $("#contact").click(function() {
        var el = "<h1>contact moi<span id='color'>.</span></h1><br />" +
        "<h5>Email:  <a href='mailto:setr710@gmail.com' id='email'>click here to write.</a></h5>";
        renderContent(el);
    });

    function renderContent(el) {
        $("#content").empty();
        $("#content").append(el);
        $("#content").hide();
        $("#content").fadeIn(1000);
    }
});

